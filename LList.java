public class LList<X>
{
    Node<X> head;
    Node<X> last;
    int count;
    
    
    public LList()
    {
        head = null;
        count = 0;
    }
    
    public void add(X s)
    {
        if (head == null)
        {
            head = new Node(s, null);
        }
        else
        {
            //Node n = head;  // Start at the head
            //while (n.next != null)  // As long as there is another Node
            //{
                // Hop to the next one
            //    n = n.next;
            //}
            Node n;
            for (n = head; n.next != null; n = n.next);
            
            // Now we are at the last Node
            n.next = new Node(s, null);
        }
        count++;
    }
    
    public void addLast(X s)
    {
        if (head == null)
        {
            head = new Node(s, null);
        }
        else
        {
            //Node n = head;  // Start at the head
            //while (n.next != null)  // As long as there is another Node
            //{
                // Hop to the next one
            //    n = n.next;
            //}
            Node n;
            for (n = head; n.next != null; n = n.next);
            
            // Now we are at the last Node
            n.next = new Node(s, null);
        }
        count++;
    }
    
    
    public void addFirst(X s)
    {
        head = new Node(s, head);
        count++;
    }
    
    
    public int size()
    {
        return count;
    }
    
    
    public X get(int index)
    {
        Node n = head;
        if (index < count && index > -1)
        {
            for (int i = 0; i < index; i++)
            {
                n = n.next;
            }
        return (X)(n.data);
        }else{
            throw new Inception();
        }
    }
    
    
    public int indexOf(X target)
    {
        Node n = head;
        if (n.data.equals(target))
            return 0;
        for (int i = 1; i < count; i++)
        {
            n = n.next;
            if (n.data.equals(target))
            return i;
        }
        return -1;
    }
    
    
    public void set(int index, X s)
    {
        if (index < count && index > -1)
        {
            Node n = head;   // Start at the head
            for (int i = 0; i < index; i++)  // Count as we go
            {
                n = n.next;  // Hop to the next Node
            }
            n.data = s;
        }
        else
        {
            throw new Inception();
        }
    }
    
    
    public void remove(int index)
    {
        if (index == 0 && count == 1)
            {
                head = null;
                count--;
            }
        else if (index == 0 && count > 1)
            {
                head = head.next;
                count--;
            }
        else if (index < count && index > -1)
        {
            Node n = head;   // Start at the head
            for (int i = 0; i < index-1; i++)  // Count as we go
            {
                n = n.next;  // Hop to the next Node
            }
            n.next = n.next.next;
            count--;
        }
        else
        {
            throw new Inception();
        }
    }
    
    public void removeLast()
    {
        if (count > 0)
    {
        Node n = head;   // Start at the head
            for (int i = 0; i < count-1; i++)  // Count as we go
            {
                n = n.next;  // Hop to the next Node
            }
        n.next = null;
        count--;
    }else{throw new Inception();}
    }
    
    public void removeFirst()
    {
        if(count>0)
    {
        if (count == 1)
    {
        head = null;
    }
        else if (count > 1)
    {
        head = head.next;
    }
    count--;
    }
        else{throw new Inception();}
    }
   
    public void clear()
    {
        head = null;
        count = 0;
    }
    
    
    public void insert(int index, X s)
    {
    if (index == 0 && index <= count)
    {
        head = new Node(s, head);
        count++;
    }
        else if (index < count && index > -1)
        {
            Node n = head;   // Start at the head
            for (int i = 0; i < index-1; i++)  // Count as we go
            {
                n = n.next;  // Hop to the next Node
            }
            n.next = new Node(s, n.next);
            count++;
        }
        else
        {
            throw new Inception();
        }
    }
    
    
    public String toString()
    {
       String result = "[]";
       if(count > 0)
    {
       Node n = head;
       result = "[" + n.data;
       for(int i = 0; i < count -1; i++)
    {   
        if (i == 0)
        result += ", ";
        n = n.next;
        if (i < count - 2)
        result += n.data + ", ";
        else
        {result += n.data;}
    }
    result += "]";
    }
    return result;
    }
}